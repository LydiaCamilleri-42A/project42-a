﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Timer : MonoBehaviour {

	public Text timerText;

	public float secs, min;

	void Start () {

		timerText = GetComponent<Text>() as Text;
	}
	

	void Update () {

		min = (int)(Time.time / 60);
		secs = (int)(Time.time % 60);

		timerText.text = "Timer: " + min.ToString("0") + ":" + secs.ToString("00");
	
	}
}
