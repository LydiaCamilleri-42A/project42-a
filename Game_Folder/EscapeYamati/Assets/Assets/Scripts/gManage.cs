﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class gManage : MonoBehaviour {

	public Sprite[] front;
	public Sprite back;
	public GameObject[] cards;
	public Text scoreText;

	private bool _init = false;
	private int _scores = 0;


	void Update () {
		if (!_init)
			initCards ();

		if (Input.GetMouseButtonUp (0))
			checkCards ();
	
	}

	void initCards(){
		for (int id = 0; id < 2; id++) {
			for (int i = 1; i < 19; i++) {

				bool test = false;
				int choice = 0;
				while (!test) {
					choice = Random.Range (0, cards.Length);
					test = !(cards [choice].GetComponent<Card> ().begin);
				}

				cards [choice].GetComponent<Card> ().cValue = i;
				cards [choice].GetComponent<Card> ().begin = true;
			}
		}

		foreach (GameObject c in cards)
			c.GetComponent<Card> ().setGraphic ();

		if (!_init)
			_init = true;
	}

	public Sprite getBack(){
		return back;
	}

	public Sprite getFront(int i){
		return front [i - 1];
	}

	void checkCards(){
		List<int> c = new List<int> ();

		for (int i = 0; i < cards.Length; i++) {
			if (cards [i].GetComponent<Card> ().stat == 1)
				c.Add (i);
		}

		if (c.Count == 2)
			cardCompare (c);
	}

	void cardCompare(List<int> c){
		Card.GAME_CARD = true;

		int x = 0;

		if(cards[c[0]].GetComponent<Card>().cValue == cards[c[1]].GetComponent<Card>().cValue){
			x = 2;
			_scores++;
			scoreText.text = "Score: " + _scores;
			if(_scores == 18)
				SceneManager.LoadScene("Card");
		}

		for  (int i = 0; i < c.Count; i++){
			cards[c[i]].GetComponent<Card>().stat = x;
			cards[c[i]].GetComponent<Card>().falseCheck();
		}
	}

}
