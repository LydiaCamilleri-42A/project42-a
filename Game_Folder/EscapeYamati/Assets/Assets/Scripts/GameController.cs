﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class GameController : MonoBehaviour {


    public int row, col,countStep;
    public int sizeRow, sizeCol;
    public int rowBlank, colBlank;

    int countPoint = 0;
    int countImageKey = 0;


    public bool startControl = false;
    public bool checkComplete;


    GameObject temp;

    public List<GameObject> imageKeyList;
    public List<GameObject> imageOfPuzzleList;
    public List<GameObject> checkPointList;

    GameObject[,] imageKeyMatrix;
    GameObject[,] imageofPuzzleMatrix;
    GameObject[,] checkPointMatrix;

    void Start()
    {
        imageKeyMatrix = new GameObject[sizeRow, sizeCol];
        imageofPuzzleMatrix = new GameObject[sizeRow, sizeCol];
        checkPointMatrix = new GameObject[sizeRow, sizeCol];


        CheckPointManager();
        ImageKeyManger();
       

    }

    void CheckPointManager()
    {
        for (int i = 0; i < sizeRow; i++)
        {

            for (int x = 0; x < sizeCol; x++)
                checkPointMatrix[i, x] = checkPointList[countImageKey];
            countImageKey++;

        }

    }


    //5/2 < VID

    void ImageKeyManger()
    {

        for (int i = 0; i < sizeRow; i++)
        {

            for (int x = 0; x < sizeCol; x++)
                imageKeyMatrix[i, x] = imageKeyList[countPoint];
            countPoint++;

        }

    }
    void Update()
    {

        if (startControl){

            startControl = false;
            if(countStep==1){

                if (imageofPuzzleMatrix[row, col] != null && imageofPuzzleMatrix[row, col].name.CompareTo("16") != 0)
                {
                    if(rowBlank != row && colBlank == col){

                       // sortImage();

                       // countStep = 0;
                    }
                
                    else if(rowBlank == row && colBlank == col){

                        //sortImage();
                        // countStep = 0;
                    }else if((rowBlank == row && colBlank == col) || (rowBlank != row && colBlank != col)){

                        countStep = 0;

                    }
                }else{
                    countStep = 0;


                }
            }
        }
    }


    void sortImage()
    {

        temp = imageofPuzzleMatrix[rowBlank, colBlank];
        imageofPuzzleMatrix[rowBlank, colBlank] = null;

        imageofPuzzleMatrix[rowBlank, colBlank] = imageofPuzzleMatrix[row, col];
        imageofPuzzleMatrix[row, col] = null;

        imageofPuzzleMatrix[row, col] = temp;

        imageofPuzzleMatrix[rowBlank, colBlank].GetComponent<imageController>().target = checkPointMatrix[rowBlank, colBlank];
        imageofPuzzleMatrix[row, col].GetComponent<imageController>().target = checkPointMatrix[row, col];

        imageofPuzzleMatrix[rowBlank, colBlank].GetComponent<imageController>().startMove = true;
        imageofPuzzleMatrix[row, col].GetComponent<imageController>().startMove = true;

        rowBlank = row;
        colBlank = col;


    }
    void ImageofLevel1()
    {

        imageofPuzzleMatrix[0, 0] = imageOfPuzzleList[4];
        imageofPuzzleMatrix[0, 1] = imageOfPuzzleList[0];
        imageofPuzzleMatrix[0, 2] = imageOfPuzzleList[1];
        imageofPuzzleMatrix[0, 3] = imageOfPuzzleList[2];
        imageofPuzzleMatrix[1, 0] = imageOfPuzzleList[8];
        imageofPuzzleMatrix[1, 1] = imageOfPuzzleList[6];
        imageofPuzzleMatrix[1, 2] = imageOfPuzzleList[7];
        imageofPuzzleMatrix[1, 3] = imageOfPuzzleList[11];
        imageofPuzzleMatrix[2, 0] = imageOfPuzzleList[12];
        imageofPuzzleMatrix[2, 1] = imageOfPuzzleList[5];
        imageofPuzzleMatrix[2, 2] = imageOfPuzzleList[14];
        imageofPuzzleMatrix[2, 3] = imageOfPuzzleList[10];
        imageofPuzzleMatrix[3, 0] = imageOfPuzzleList[13];
        imageofPuzzleMatrix[3, 1] = imageOfPuzzleList[9];
        imageofPuzzleMatrix[3, 2] = imageOfPuzzleList[15];
        imageofPuzzleMatrix[3, 3] = imageOfPuzzleList[3];


    }
}
