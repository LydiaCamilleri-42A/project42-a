﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class Card : MonoBehaviour {

	public static bool GAME_CARD = false;

	[SerializeField]
	private int _stat;
	[SerializeField]
	private int _value;
	[SerializeField]
	private bool _start = false;

	private Sprite _back;
	private Sprite _front;

	private GameObject _manage;

	void Start(){
		_stat = 1;
		_manage = GameObject.FindGameObjectWithTag ("Manage");
	}

	public void setGraphic(){
		_back = _manage.GetComponent<gManage> ().getBack ();
		_front = _manage.GetComponent<gManage> ().getFront (_value);

		flipC ();
	}

	public void flipC(){

		if (_stat == 0)
			_stat = 1;
		else if (_stat == 1)
			_stat = 0;
		
		if (_stat == 0 && !GAME_CARD)
			GetComponent<Image> ().sprite = _back;
		else if (_stat == 1 && !GAME_CARD)
			GetComponent<Image> ().sprite = _front;
	}

	public int cValue{
		get{ return _value;}
		set{ _value = value;}
	}

	public int stat{
		get{ return _stat;}
		set{ _stat = value;}
	}

	public bool begin{
		get{ return _start;}
		set{ _start = value;}
	}

	public void falseCheck(){
		StartCoroutine (pause ());
	}

	IEnumerator pause(){
		yield return new WaitForSeconds (1);
		if (_stat == 0)
			GetComponent<Image> ().sprite = _back;
		else if (_stat == 1)
			GetComponent<Image> ().sprite = _front;

		GAME_CARD = false;
	}
}
