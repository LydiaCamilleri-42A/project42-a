﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class screenLayoutController : MonoBehaviour
{

	GameObject theSquare;
	Vector3 bottomLeftPosition;
	bool animating;

	// Use this for initialization
	void Start ()
	{
		animating = false;
		//to get the reference to the square
		theSquare = GameObject.Find ("mySquare");
		Debug.Log (Screen.width + "X" + Screen.height);

		bottomLeftPosition = Camera.main.ScreenToWorldPoint (
			new Vector3 (0f, 0f));
		StartCoroutine (animateBox ());
	}

	IEnumerator animateBox(){
		while (true) {
			if (animating) {
				//do the animation
				bottomLeft();
				yield return new WaitForSeconds (1f);
				topLeft ();
				yield return new WaitForSeconds (1f);
				topCenter ();
				yield return new WaitForSeconds (1f);
				topRight ();
				yield return new WaitForSeconds (1f);
				bottomRight ();
				yield return new WaitForSeconds (1f);
				bottomCenter ();
				yield return new WaitForSeconds (1f);
				middle ();
				yield return new WaitForSeconds (1f);

			}
			yield return null;
		}

	}

	void Update ()
	{
		if (Input.GetKeyDown (KeyCode.L)) {
			Debug.Log ("animating value: " + animating);
			animating = !animating;
		}

		if (Input.GetKeyDown (KeyCode.Z)) {
			bottomLeft ();
		}

		if (Input.GetKeyDown (KeyCode.X)) {
			topLeft ();
		}

		if (Input.GetKeyDown (KeyCode.C)) {
			topRight ();
		}

		if (Input.GetKeyDown (KeyCode.V)) {
			bottomRight ();
		}

		if (Input.GetKeyDown (KeyCode.B)) {
			topCenter ();
		}

		if (Input.GetKeyDown (KeyCode.N)) {
			bottomCenter ();
		}

		if (Input.GetKeyDown (KeyCode.Space)) {
			middle ();
		}


		if (Input.GetKeyDown (KeyCode.LeftArrow)) {
			theSquare.transform.position -= new Vector3 (1f, 0f);
		}

		if (Input.GetKeyDown (KeyCode.RightArrow)) {
			theSquare.transform.position += new Vector3 (1f, 0f);
		}

		if (Input.GetKeyDown (KeyCode.UpArrow)) {
			theSquare.transform.position += new Vector3 (0f, 1f);
		}

		if (Input.GetKeyDown (KeyCode.DownArrow)) {
			theSquare.transform.position -= new Vector3 (0f, 1f);
		}


	}



	void middle ()
	{
		theSquare.transform.position = new Vector3 (0f, 0f);
	}

	void bottomLeft ()
	{
		Debug.Log ("Bottom Left");
		theSquare.transform.position = new Vector3 (
			bottomLeftPosition.x, bottomLeftPosition.y);
		theSquare.transform.position += new Vector3 (0.5f, 0.5f);
	}

	void topLeft()
	{
		Debug.Log ("Top Left");
		theSquare.transform.position = new Vector3 (
			bottomLeftPosition.x, -bottomLeftPosition.y);
		theSquare.transform.position += new Vector3 (0.5f, -0.5f);
	}

	void topRight ()
	{
		Debug.Log ("Top Right");
		theSquare.transform.position = new Vector3 (
			-bottomLeftPosition.x, -bottomLeftPosition.y);
		theSquare.transform.position += new Vector3 (-0.5f, -0.5f);
	}

	void bottomRight ()
	{
		Debug.Log ("Bottom Right");
		theSquare.transform.position = new Vector3 (
			-bottomLeftPosition.x, bottomLeftPosition.y);
		theSquare.transform.position += new Vector3 (-0.5f, 0.5f);
	}

	void topCenter()
	{
		Debug.Log ("Top Center");
		theSquare.transform.position = new Vector3 (
			0f, -bottomLeftPosition.y);
		theSquare.transform.position += new Vector3 (0f, -0.5f);
	}

	void bottomCenter()
	{
		Debug.Log ("Bottom Center");
		theSquare.transform.position = new Vector3 (
			0f, bottomLeftPosition.y);
		theSquare.transform.position += new Vector3 (0f, 0.5f);
	}

		
	
	// Update is called once per frame

}
